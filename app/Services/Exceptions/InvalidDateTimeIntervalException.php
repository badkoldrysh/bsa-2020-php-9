<?php

declare(strict_types=1);

namespace App\Services\Exceptions;

class InvalidDateTimeIntervalException extends \InvalidArgumentException
{
	const ERROR_MESSAGE = 'endDate must be greater than startDate';

	public function __construct()
	{
		parent::__construct(self::ERROR_MESSAGE);
	}
}
