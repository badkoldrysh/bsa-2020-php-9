<?php

namespace Tests\Unit;

use App\DTO\Collections\ChartDataCollection;
use App\DTO\Values\ChartData;
use App\Entities\Stock;
use App\Repositories\StockRepository;
use App\Services\Exceptions\InvalidFrequencyException;
use DateTime;
use Illuminate\Support\Carbon;
use Tests\TestCase;
use App\Services\MarketDataService;

class MarketServiceTest extends TestCase
{
    const DATE_TIME_FORMAT = 'Y-m-d H:i:s';

    public function provide_getChartData_frequences()
    {
        return [
            'null-frequence' => [0],
            'negative-frequence' => [-1],
        ];
    }

    /**
     * @dataProvider provide_getChartData_frequences
     */
    public function test_getChartData_exception(int $frequency)
    {
        $startDate = Carbon::create(15, 0, 0);
        $endDate = Carbon::create(16, 0, 0);

        $stockRepository = $this->mock(StockRepository::class);

        $marketServiceMock = new MarketDataService($stockRepository);

        $this->expectException(InvalidFrequencyException::class);
        $this->expectExceptionMessage(InvalidFrequencyException::ERROR_MESSAGE);
        $marketServiceMock->getChartData($startDate, $endDate, $frequency);
    }


    public function test_getChartData_returns_empty_data()
    {
        $startDate = Carbon::create(15, 0, 0);
        $endDate = Carbon::create(16, 0, 0);
        $frequency = 1;

        $stockRepository = $this->mock(
            StockRepository::class,
            function ($mock) {
                $mock->shouldReceive('findByCriteria')
                    ->once()
                    ->andReturn(collect([]));
            }
        );

        $marketServiceMock = new MarketDataService($stockRepository);

        $this->assertEquals(
            new ChartDataCollection(),
            $marketServiceMock->getChartData($startDate, $endDate, $frequency)
        );
    }

    public function provide_getChartData_hour_correctly(): array
    {
        $halfHourCollection = new ChartDataCollection(
            [
                new ChartData(new DateTime('2020-07-23 12:00:00'), 150),
                new ChartData(new DateTime('2020-07-23 12:30:00'), 350),
                new ChartData(new DateTime('2020-07-23 13:00:00'), 500),
                new ChartData(new DateTime('2020-07-23 13:30:00'), 600),
                new ChartData(new DateTime('2020-07-23 14:00:00'), 700),
            ]
        );

        $hourCollection = new ChartDataCollection(
            [
                new ChartData(new DateTime('2020-07-23 12:00:00'), 250),
                new ChartData(new DateTime('2020-07-23 13:00:00'), 550),
                new ChartData(new DateTime('2020-07-23 14:00:00'), 700),
            ]
        );

        $twoHourCollection = new ChartDataCollection(
            [
                new ChartData(new DateTime('2020-07-23 12:00:00'), 350),
                new ChartData(new DateTime('2020-07-23 14:00:00'), 700),
            ]
        );

        return [
            'half-hour' => [1800, $halfHourCollection],
            'hour' => [3600, $hourCollection],
            'two-hour' => [7200, $twoHourCollection]
        ];
    }

    /**
     * @dataProvider provide_getChartData_hour_correctly
     * @param int $frequency
     * @param int $price
     * @param DateTime $date
     */
    public function test_getChartData_returns_data_correctly_by_hour(int $frequency, ChartDataCollection $expectedCollection)
    {
        $startDate = new DateTime('2020-07-23 12:00:00');
        $endDate = new DateTime('2020-07-23 14:00:00');

        $dates = [
            new DateTime('2020-07-23 12:10:00'),
            new DateTime('2020-07-23 12:15:00'),
            new DateTime('2020-07-23 12:30:00'),
            new DateTime('2020-07-23 12:45:00'),
            new DateTime('2020-07-23 13:00:00'),
            new DateTime('2020-07-23 13:30:00'),
            new DateTime('2020-07-23 14:00:00'),
        ];

        $stockRepository = $this->mock(
            StockRepository::class,
            function ($mock) use ($dates) {
                foreach ($dates as $key => $date) {
                    $dates[$key] = $date->format(self::DATE_TIME_FORMAT);
                }
                $mock->shouldReceive('findByCriteria')
                    ->once()
                    ->andReturn(
                        collect(
                            [
                                self::stock(1, 100, $dates[0]),
                                self::stock(2, 200, $dates[1]),
                                self::stock(3, 300, $dates[2]),
                                self::stock(4, 400, $dates[3]),
                                self::stock(5, 500, $dates[4]),
                                self::stock(6, 600, $dates[5]),
                                self::stock(7, 700, $dates[6]),
                            ]
                        )
                    );
            }
        );
        $marketServiceMock = new MarketDataService($stockRepository);

        $this->assertEquals(
            $expectedCollection,
            $marketServiceMock->getChartData($startDate, $endDate, $frequency)
        );
    }

    public function provide_getChartData_days_correctly(): array
    {
        $oneDayCollection = new ChartDataCollection(
            [
                new ChartData(new DateTime('2020-07-23 00:00:00'), 100),
                new ChartData(new DateTime('2020-07-24 00:00:00'), 200),
            ]
        );

        $twoDaysCollection = new ChartDataCollection(
            [
                new ChartData(new DateTime('2020-07-23 00:00:00'), 150),
            ]
        );

        return [
            'one-day' => [86400, $oneDayCollection],
            'two-days' => [172800, $twoDaysCollection],
        ];
    }


    /**
     * @dataProvider provide_getChartData_days_correctly
     * @param int $frequency
     * @param int $price
     * @param DateTime $date
     */
    public function test_getChartData_returns_data_correctly_by_days(int $frequency, ChartDataCollection $expectedCollection)
    {
        $startDate = new DateTime('2020-07-23 00:00:00');
        $endDate = new DateTime('2020-07-25 00:00:00');

        $dates = [
            new DateTime('2020-07-23 12:10:00'),
            new DateTime('2020-07-23 12:15:00'),
            new DateTime('2020-07-23 12:30:00'),
            new DateTime('2020-07-23 11:30:00'),
            new DateTime('2020-07-24 00:45:00'),
            new DateTime('2020-07-24 13:00:00'),
            new DateTime('2020-07-24 13:30:00'),
            new DateTime('2020-07-24 17:21:05'),
        ];

        $stockRepository = $this->mock(
            StockRepository::class,
            function ($mock) use ($dates) {
                foreach ($dates as $key => $date) {
                    $dates[$key] = $date->format(self::DATE_TIME_FORMAT);
                }
                $mock->shouldReceive('findByCriteria')
                    ->once()
                    ->andReturn(
                        collect(
                            [
                                self::stock(1, 100, $dates[0]),
                                self::stock(2, 100, $dates[1]),
                                self::stock(3, 100, $dates[2]),
                                self::stock(4, 100, $dates[3]),
                                self::stock(5, 200, $dates[4]),
                                self::stock(6, 200, $dates[5]),
                                self::stock(7, 200, $dates[6]),
                                self::stock(8, 200, $dates[7]),
                            ]
                        )
                    );
            }
        );
        $marketServiceMock = new MarketDataService($stockRepository);

        $this->assertEquals(
            $expectedCollection,
            $marketServiceMock->getChartData($startDate, $endDate, $frequency)
        );
    }

    public function provide_getChartData_weeks_correctly(): array
    {
        $oneWeekCollection = new ChartDataCollection(
            [
                new ChartData(new DateTime('2020-07-01 00:00:00'), 100),
                new ChartData(new DateTime('2020-07-08 00:00:00'), 200),
                new ChartData(new DateTime('2020-07-15 00:00:00'), 300),
            ]
        );

        $twoWeeksCollection = new ChartDataCollection(
            [
                new ChartData(new DateTime('2020-07-01 00:00:00'), 150),
                new ChartData(new DateTime('2020-07-15 00:00:00'), 300),
            ]
        );

        $threeWeeksCollection = new ChartDataCollection(
            [
                new ChartData(new DateTime('2020-07-01 00:00:00'), 200),
            ]
        );

        return [
            'one-week' => [604800, $oneWeekCollection],
            'two-weeks' => [1209600, $twoWeeksCollection],
            'three-weeks' => [1814400, $threeWeeksCollection],
        ];
    }

    /**
     * @dataProvider provide_getChartData_weeks_correctly
     * @param int $frequency
     * @param int $price
     * @param DateTime $date
     */
    public function test_getChartData_returns_data_correctly_by_weeks(int $frequency, ChartDataCollection $expectedCollection)
    {
        $startDate = new DateTime('2020-07-01 00:00:00');
        $endDate = new DateTime('2020-07-22 00:00:00');

        $dates = [
            new DateTime('2020-07-01 12:10:00'),
            new DateTime('2020-07-05 12:15:00'),
            new DateTime('2020-07-06 12:30:00'),
            new DateTime('2020-07-07 11:30:00'),
            new DateTime('2020-07-08 00:45:00'),
            new DateTime('2020-07-11 13:00:00'),
            new DateTime('2020-07-12 13:30:00'),
            new DateTime('2020-07-14 17:21:05'),
            new DateTime('2020-07-15 00:45:00'),
            new DateTime('2020-07-16 06:48:10'),
            new DateTime('2020-07-16 09:33:11'),
            new DateTime('2020-07-20 11:21:05'),
        ];

        $stockRepository = $this->mock(
            StockRepository::class,
            function ($mock) use ($dates) {
                foreach ($dates as $key => $date) {
                    $dates[$key] = $date->format(self::DATE_TIME_FORMAT);
                }
                $mock->shouldReceive('findByCriteria')
                    ->once()
                    ->andReturn(
                        collect(
                            [
                                self::stock(1, 100, $dates[0]),
                                self::stock(2, 100, $dates[1]),
                                self::stock(3, 100, $dates[2]),
                                self::stock(4, 100, $dates[3]),
                                self::stock(5, 200, $dates[4]),
                                self::stock(6, 200, $dates[5]),
                                self::stock(7, 200, $dates[6]),
                                self::stock(8, 200, $dates[7]),
                                self::stock(5, 300, $dates[8]),
                                self::stock(6, 300, $dates[9]),
                                self::stock(7, 300, $dates[10]),
                                self::stock(8, 300, $dates[11]),
                            ]
                        )
                    );
            }
        );
        $marketServiceMock = new MarketDataService($stockRepository);

        $this->assertEquals(
            $expectedCollection,
            $marketServiceMock->getChartData($startDate, $endDate, $frequency)
        );
    }



    private static function stock(int $user_id, int $price, string $start_date): Stock
    {
        $stock = new Stock();
        $stock->user_id = $user_id;
        $stock->price = $price;
        $stock->start_date = $start_date;

        return $stock;
    }
}
