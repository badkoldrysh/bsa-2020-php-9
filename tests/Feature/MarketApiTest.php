<?php

namespace Tests\Feature;

use App\Entities\Stock;
use App\Entities\User;
use DateTime;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class MarketApiTest extends TestCase
{
    use DatabaseMigrations;
    use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_postStock()
    {
        $user_id = 5;
        $stock_id = 1;
        $price = 100;
        $user = factory(User::class)->make(['id' => $user_id]);
        $user->save();
        $stockDate = new DateTime('now');

        $response = $this->actingAs($user)->json(
            "POST",
            '/api/stocks',
            [
                "price" => $price,
                "start_date" => $stockDate->format('Y-m-d H:i:s'),
            ]
        );

        $response->assertStatus(201);
        $response->assertJson(
            [
                'data' => [
                    'id' => $stock_id,
                    'price' => $price,
                    'start_date' => $stockDate->format('Y-m-d H:i:s')
                ]
            ]
        );
    }

    public function test_deleteStock()
    {
        $user_id = 5;
        $user = factory(User::class)->make(['id' => $user_id]);
        $user->save();

        $stock = factory(Stock::class)->make(['user_id' => $user_id]);
        $stock->save();

        $response = $this->actingAs($user)->delete(
            '/api/stocks/' . $stock->id
        );

        $response->assertStatus(204);
    }

    public function test_getStocks()
    {
        $start_date = (new DateTime('2020-07-01 00:00:00'))->getTimestamp();
        $end_date = (new DateTime('now'))->getTimestamp();
        $frequency = 3600;
        $user_id = 5;
        $user = factory(User::class)->make(['id' => $user_id]);
        $user->save();
        $stocks = factory(Stock::class, 5)->make(['user_id' => $user_id]);
        foreach ($stocks as $stock) {
            $stock->save();
        }

        $query = http_build_query(
            [
                'start_date' => $start_date,
                'end_date' => $end_date,
                'frequency' => $frequency,
            ],
        );

        $response = $this->actingAs($user)->get(
            '/api/chart-data?' . $query,
            [
                [
                    "content-type" => "application/json"
                ]
            ]
        );

        $response->assertStatus(200);
        $response->assertJsonStructure(
            [
                'data' => [
                    '*' => [
                        'price',
                        'date',
                    ]
                ]
            ]
        );
    }
    public function test_getStocks_unauthorized()
    {
        $start_date = (new DateTime('2020-07-01 00:00:00'))->getTimestamp();
        $end_date = (new DateTime('now'))->getTimestamp();
        $frequency = 3600;
        $user_id = 5;
        $user = factory(User::class)->make(['id' => $user_id]);
        $user->save();
        $stocks = factory(Stock::class, 5)->make(['user_id' => $user_id]);
        foreach ($stocks as $stock) {
            $stock->save();
        }

        $query = http_build_query(
            [
                'start_date' => $start_date,
                'end_date' => $end_date,
                'frequency' => $frequency,
            ],
        );

        $response = $this->get(
            '/api/chart-data?' . $query,
            [
                [
                    "content-type" => "application/json"
                ]
            ]
        );

        $response->assertStatus(200);
        $response->assertJsonStructure(
            [
                'data' => [
                    '*' => [
                        'price',
                        'date',
                    ]
                ]
            ]
        );
    }

    /**
     * @testWith    [0, -1]
     * @param int $frequency
     */
    public function test_getStocks_negative_frequency(int $frequency)
    {
        $start_date = (new DateTime('2020-07-01 00:00:00'))->getTimestamp();
        $end_date = (new DateTime('now'))->getTimestamp();
        $user_id = 5;
        $user = factory(User::class)->make(['id' => $user_id]);

        $query = http_build_query(
            [
                'start_date' => $start_date,
                'end_date' => $end_date,
                'frequency' => $frequency,
            ],
        );

        $response = $this->actingAs($user)->get(
            '/api/chart-data?' . $query,
            [
                [
                    "content-type" => "application/json"
                ]
            ]
        );

        $response->assertStatus(400);
    }

    public function test_getStocks_bad_interval()
    {
        $start_date = (new DateTime('2021-07-01 00:00:00'))->getTimestamp();
        $end_date = (new DateTime('2020-07-01 00:00:00'))->getTimestamp();
        $user_id = 5;
        $frequency = 3600;
        $user = factory(User::class)->make(['id' => $user_id]);

        $query = http_build_query(
            [
                'start_date' => $start_date,
                'end_date' => $end_date,
                'frequency' => $frequency,
            ],
        );

        $response = $this->actingAs($user)->get(
            '/api/chart-data?' . $query,
            [
                [
                    "content-type" => "application/json"
                ]
            ]
        );

        $response->assertStatus(400);
    }

    public function test_getStocks_negative_price()
    {
        $user_id = 5;
        $price = -100;
        $user = factory(User::class)->make(['id' => $user_id]);
        $stockDate = new DateTime('now');

        $response = $this->actingAs($user)->json(
            "POST",
            '/api/stocks',
            [
                "price" => $price,
                "startDate" => $stockDate->format('Y-m-d H:i:s'),
            ]
        );

        $response->assertStatus(400);
    }

    public function test_getStocks_another_user()
    {
        $start_date = (new DateTime('2020-07-01 00:00:00'))->getTimestamp();
        $end_date = (new DateTime('now'))->getTimestamp();
        $frequency = 3600;

        $user_id = 1;
        $another_user_id = 2;
        $user = factory(User::class)->make(['id' => $user_id]);
        $another_user = factory(User::class)->make(['id' => $another_user_id]);

        $user->save();
        $another_user->save();

        $stocks = factory(Stock::class, 5)->make(['user_id' => $user_id]);
        foreach ($stocks as $stock) {
            $stock->save();
        }

        $query = http_build_query(
            [
                'start_date' => $start_date,
                'end_date' => $end_date,
                'frequency' => $frequency,
            ],
        );

        $response = $this->actingAs($user)->get(
            '/api/chart-data?' . $query,
            [
                [
                    "content-type" => "application/json"
                ]
            ]
        );

        $response->assertStatus(200);
    }

    public function test_deleteStock_by_another_user()
    {
        $user_id = 1;
        $another_user_id = 2;
        $user = factory(User::class)->make(['id' => $user_id]);
        $user->save();
        $another_user = factory(User::class)->make(['id' => $another_user_id]);
        $another_user->save();

        $stock = factory(Stock::class)->make(['user_id' => $user_id]);
        $stock->save();

        $response = $this->actingAs($another_user)->delete(
            '/api/stocks/' . $stock->id
        );

        $response->assertStatus(404);
    }

    public function test_postStock_past_date()
    {
        $user_id = 1;
        $price = 100;
        $user = factory(User::class)->make(['id' => $user_id]);
        $user->save();
        $stockDate = new DateTime('now -1 month');

        $response = $this->actingAs($user)->json(
            "POST",
            '/api/stocks',
            [
                "price" => $price,
                "start_date" => $stockDate->format('Y-m-d H:i:s'),
            ]
        );

        $response->assertStatus(400);
    }
}
